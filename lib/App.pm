package App;
use strict;
use warnings;
use utf8;

use Rutracker;
use Files;
use Omxplayer;
use Transmission;

sub new {
    my ($class, %params) = @_;

    my $rt          = Rutracker->new(username => $params{rt_username}, password => $params{rt_password});
    my $omxplayer   = Omxplayer->new(home => $params{home_dir});
    my $files       = Files->new(dir => $params{files_dir}, omxplayer => $omxplayer);

    return bless {
        _rt         => $rt,
        _files      => $files,
        _omxplayer  => $omxplayer,
    }, $class;
}

sub list_files {
    my ($self) = @_;
    return $self->{_files}->list_files();
}

sub play {
    my ($self, $file) = @_;

    return {response => $self->{_files}->play($file)};
}
sub control_play {
    my ($self, $command) = @_;

    $self->{_omxplayer}->control_play($command);

    return {response => 'ok'};
}


1;
