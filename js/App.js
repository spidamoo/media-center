let is_playing;

$(function() {

    let _files_check            = $.Deferred();
    let _torrents_check         = $.Deferred();
    let _playback_history_check = $.Deferred();

    $('#tabs>div.tab').click(function() {
        $('#tabs>div.tab.active').each(function() {
            $( '#' + $(this).data('target') ).hide();
        });
        $('#tabs>div.tab').removeClass('active');
        $(this).addClass('active');
        $( '#' + $(this).data('target') ).show();
    });

    $('#tabs>div.tab').first().click();

    $('#downloads').on('click', 'button.play', function() {
        const file = $(this).parents('.downloads_entry').find('.file').text();
        play(file);
    });
    $('#downloads').on('click', 'button.pause', function() {
        pause();
    });
    $('#downloads').on('click', 'button.stop', function() {
        stop();
    });

    check_files();
    check_torrents();
    check_playback_history();
});

function check_files() {
    $.get('list_files', function(data) {
        console.log('files', data);

        $('#downloads').empty();

        const template = $('.template.downloads_entry');
        for (const file of data) {
            let html = template[0].outerHTML;
            for (const prop in file) {
                html = html.replace('{{' + prop + '}}', file[prop]);
            }

            const $file = $(html);
            $file.removeClass('template');
            $('#downloads').append($file);
        }
    });
}
function check_torrents() {}
function check_playback_history() {}

function play(file) {
    $.get('play/' + file);
    set_playing(file);
}
function pause() {
    $.get('control_play/pause');
}
function stop() {
    $.get('control_play/stop');
    set_playing(false);
}
function set_playing(file) {
    is_playing = file;
    if (file) {
        // $('.playback_control').show();
        // $('.play').hide();
    }
    else {
        // $('.playback_control').hide();
        // $('.play').show();
    }
}
