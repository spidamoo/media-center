package Rutracker;
use strict;
use warnings;
use utf8;

use LWP::UserAgent;
use HTML::TreeBuilder 5 -weak;
use Encode qw(encode_utf8);

sub new {
    my ($class, %params) = @_;

    my $ua = LWP::UserAgent->new();

    return bless {
        _ua => $ua,
        username => $params{username},
        password => $params{password},
    }, $class;
}

sub login {
    my ($self) = @_;

    my $username = $self->{username};
    my $password = $self->{password};
    my $url = 'https://rutracker.org/forum/login.php';
    my %form = (
        login_username  => $username,
        login_password  => $password,
        login           => 'Вход',
    );
    my $res = $self->{_ua}->post($url, \%form);

    if ($res->code() eq '302') {
        $self->{_cookies} = [$res->header('set-cookie')];
    }
    else {
        die 'login failed: ', $res->status_line();
    }

    return $self;
}

sub search {
    my ($self, $what) = @_;

    my @search_results;

    my $url = "https://rutracker.org/forum/tracker.php?nm=$what";

    while (1) {
        my $search_response = $self->_request_search_page($url);
        push @search_results, @{ $search_response->{results} };

        last unless $search_response->{next_url};
        $url = 'https://rutracker.org/forum/' . $search_response->{next_url};

        printf "next page: %s\n", $url;
    }

    # printf( "%20s %100s\n", encode_utf8($_->{forum}), encode_utf8($_->{title}) ) for @search_results;

    # print STDERR eDumper(\@search_results);
    printf "found %d results\n", scalar(@search_results);

    return \@search_results;
}

sub _request_search_page {
    my ($self, $url) = @_;

    my $res = $self->{_ua}->post( $url, {}, Cookie => join(';', @{ $self->{_cookies} }) );

    if ($res->code() eq '200') {
        my $doc = HTML::TreeBuilder->new_from_content( $res->decoded_content() );
        my $search_results_root = $doc->find_by_attribute('id' => 'search-results')->find('tbody');

        my @search_results;

        for my $row( $search_results_root->find('tr') ) {
            eval {
                push @search_results, {
                    forum   => $row->look_down(class => qr/f-name/  )->find('a')->as_text(),
                    title   => $row->look_down(class => qr/t-title/ )->find('a')->as_text(),
                    url     => $row->look_down(class => qr/t-title/ )->find('a')->attr('href'),
                    seeders => $row->look_down(class => qr/seedmed/ )->as_text(),
                    size    => $row->look_down(class => qr/tor-size/)->find('a')->as_text() =~ /([\d+\.]+\s\w+)/,
                };
            } or do {
                warn sprintf( 'could not parse: %s', encode_utf8( $row->as_HTML() ) );
            };
        }

        my $next_link = $doc->look_down(_tag => 'a', class => qr/pg/, sub {$_[0]->as_text() eq 'След.'});

        return {
            results     => \@search_results,
            next_url    => $next_link && $next_link->attr('href'),
        };
    }
    else {
        die 'search failed: ', $res->status_line();
    }
}


1;
