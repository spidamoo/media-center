#!/usr/bin/env perl

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/lib";

use Plack::Runner;
use Plack::Builder;
use Web::Hippie;

use AnyEvent;
use Time::HiRes qw(time);
# use IO::All;
use Cpanel::JSON::XS;
use Config::Tiny;
# use Template;

use App;

use constant PLACK_ENV => $ENV{PLACK_ENV} || 'development'; # this is not accurate as PLACK_ENV can be later affected by -E arg
use constant DEBUG     => $ENV{DEBUG}     // PLACK_ENV eq 'development';

printf "running in %s mode\n", PLACK_ENV;

use if DEBUG, 'Data::Dumper::AutoEncode';

my %static_files = (
    '/'                             => {fn => 'index.html', headers => ['Content-Type' => 'text/html; charset=utf-8']},
    '/css/styles.css'               => 0,

    '/js/jquery-3.2.1.min.js'       => 0,
    '/js/App.js'                    => 0,

    '/favicon/apple-icon-57x57.png'     => 0,
    '/favicon/apple-icon-60x60.png'     => 0,
    '/favicon/apple-icon-72x72.png'     => 0,
    '/favicon/apple-icon-76x76.png'     => 0,
    '/favicon/apple-icon-114x114.png'   => 0,
    '/favicon/apple-icon-120x120.png'   => 0,
    '/favicon/apple-icon-144x144.png'   => 0,
    '/favicon/apple-icon-152x152.png'   => 0,
    '/favicon/apple-icon-180x180.png'   => 0,
    '/favicon/android-icon-192x192.png' => 0,
    '/favicon/favicon-32x32.png'        => 0,
    '/favicon/favicon-96x96.png'        => 0,
    '/favicon/favicon-16x16.png'        => 0,
    '/favicon/manifest.json'            => 0,
    '/favicon/ms-icon-144x144.png'      => 0,
);
my %html = ();


if (PLACK_ENV eq 'development') {
    # my $tt = Template->new({});
    # while (my ($from, $to) = each %html) {
    #     # $tt->process($from, undef, $to);
    #     my $text = io($from)->slurp();
    #     $text =~ s/\[%(.+?)%\]/eval $1/eg;
    #     io($to)->print($text);
    # }
}

my $config = Config::Tiny->read('config.conf');

my $mc_app = App->new(
    files_dir   => $config->{_}{files_dir},
    home_dir    => $config->{_}{home_dir},
    rt_username => $config->{_}{rt_username},
    rt_password => $config->{_}{rt_password},
);

# my $dt = 0.03;
# my $prev = time();
# my $start = time();
# my $now;
# my $ticker = AE::timer($dt, $dt, sub {
#     $now = time();
#     $dt = $now - $prev;
#     # print STDERR $now - $start, "\t", $dt, "\n";
#     $prev = $now;
#     $game->step($dt);
# });


my $app = builder {
    # mount '/_hippie' => builder {
    #     enable "+Web::Hippie";

    #     sub {
    #         my $env = shift;
    #         my $client_id = $env->{'hippie.client_id'};
    #         my $handle    = $env->{'hippie.handle'};
    #         my $path      = $env->{PATH_INFO};

    #         if ($path eq '/init') {
    #         }
    #         elsif ($path eq '/message') {
    #             my $messages = $env->{'hippie.message'};
    #             $messages = [$messages] unless (ref($messages) eq 'ARRAY');
    #             for my $message(@$messages) {
    #                 $game->process_message($client_id, $message);
    #             }
    #         }
    #         elsif ($path eq '/error') {
    #         }
    #         elsif ($path eq '/disconnect') {
    #         }
    #     }
    # };
    mount '/' => sub {
        my $env = shift;

        if (exists $static_files{ $env->{PATH_INFO} }) {
            my $fn = $static_files{ $env->{PATH_INFO} } ? $static_files{ $env->{PATH_INFO} }->{fn} : './' . $env->{PATH_INFO};

            if (-e $fn) {
                my $headers = $static_files{ $env->{PATH_INFO} } ? $static_files{ $env->{PATH_INFO} }->{headers} : [];
                open my $fh, '<' . $fn;
                return [200, $headers, $fh];
            }
        }

        my (undef, $command, @params) = split('/', $env->{PATH_INFO});
        if ( $mc_app->can($command) ) {
            my $response = $mc_app->$command(@params);

            return [
                200,
                ['Content-Type' => 'application/json; charset=utf-8'],
                [encode_json($response)]
            ];
        }


        return [
            404,
            ['Content-Type' => 'text/plain; charset=utf-8'],
            ['Здесь рыбы нет']
        ];
    };
};

my $runner = Plack::Runner->new();
$runner->parse_options(@ARGV);
$runner->run($app);

