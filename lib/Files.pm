package Files;
use strict;
use warnings;
use utf8;

sub new {
    my ($class, %params) = @_;

    return bless {
        _dir            => $params{dir},
        _omxplayer      => $params{omxplayer},
        _known_files    => {},
    }, $class;
}

sub list_files {
    my ($self) = @_;

    my @entries;

    opendir(my $dh, $self->{_dir}) or die "Can't opendir $self->{_dir}: $!";
    while ( my $file = readdir($dh) ) {
        next if $file =~ /^\./;

        my $entry;
        if (-d $self->{_dir} . '/' . $file) {
            my $episodes = $self->_list_episodes($self->{_dir} . '/' . $file);
            $entry = {
                type        => 'series',
                name        => $file,
                episodes    => $episodes,
            };
        }
        else {
            next unless _file_is_video($file);
            my $tracks = $self->{_omxplayer}->list_tracks($self->{_dir} . '/' . $file);
            $entry = {
                type    => 'movie',
                name    => $file,
                tracks  => $tracks,
            };
        }

        push @entries, $entry if $entry;

        $self->{_known_files}{$file} = $entry;
    }

    return \@entries;
}

sub play {
    my ($self, $file, $episode) = @_;
    $episode //= 0;

    return 'no such file' unless $self->{_known_files}{$file};

    if ($self->{_known_files}{$file}{type} eq 'movie') {
        $self->{_omxplayer}->play($self->{_dir} . '/' . $file);
    }
    elsif ($self->{_known_files}{$file}{type} eq 'series') {
        $self->{_omxplayer}->play($self->{_dir} . '/' . $file . '/' . $self->{_known_files}{$file}{episodes}[0]{name});
    }
    return 'ok';
}

sub _list_episodes {
    my ($self, $dir) = @_;

    my @episodes;

    opendir(my $dh, $dir) or die "Can't opendir $dir: $!";
    while ( my $file = readdir($dh) ) {
        next if $file =~ /^\./;
        next unless _file_is_video($file);

        push @episodes, {name => $file};
    }

    @episodes = sort { $a->{name} cmp $b->{name} } @episodes;

    $episodes[0]->{tracks} = $self->{_omxplayer}->list_tracks($dir . '/' . $episodes[0]->{name});

    return \@episodes;
}

sub _file_is_video {
    my ($fn) = @_;

    return $fn =~ /\.(?:mkv|mp4|flv|avi)$/
}


1;
