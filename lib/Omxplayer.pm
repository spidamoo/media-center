package Omxplayer;
use strict;
use warnings;
use utf8;

use Data::Dumper;
use IO::Handle;

sub new {
    my ($class, %params) = @_;

    return bless {
        _control_file   => $params{home} . '/omxplayer_control.txt',
        _out_file       => $params{home} . '/omxplayer_out.txt',
        _file           => $params{home} . '/omxplayer_.txt',
    }, $class;
}

sub list_tracks {
    my ($self, $file) = @_;

    my $output = `omxplayer -i '$file' 2>&1`;
    print STDERR "list_tracks $file\n";
#     my $output = q{Input #0, matroska,webm, from 'Heldensagen vom Kosmosinsel Ginga Eiyuu Densetsu Waga Yuku wa Hoshi no Taikai (BDRemux 1920x1080 AVC FLAC).mkv':
#   Metadata:
#     title           : Heldensagen vom Kosmosinsel 銀河英雄伝説 わが征くは星の大海
#     encoder         : libebml v1.2.3 + libmatroska v1.3.0
#     creation_time   : 2012-03-28T08:10:46.000000Z
#   Duration: 00:59:30.57, start: 0.000000, bitrate: 33277 kb/s
#     Stream #0:0: Video: h264 (High), yuv420p(progressive), 1920x1080 [SAR 1:1 DAR 16:9], 23.98 fps, 23.98 tbr, 1k tbn, 47.95 tbc (default)
#     Stream #0:1(jpn): Audio: flac, 48000 Hz, stereo, s16 (default)
#     Metadata:
#       title           : LPCM->FLAC
#     Stream #0:2(rus): Subtitle: subrip (default)
#     Metadata:
#       title           : Stars & Swords
# have a nice day ;)};

    my $tracks = {audio => [], subtitle => []};
    my @streams = ($output =~ /Stream #\d+:\d+\(\w+?\): \w+?: .+?\n/gs);

    for my $stream(@streams) {
        my ($language, $type, $name) = ($stream =~ /Stream #\d+:\d+\((\w+?)\): (\w+?): (.+?)\n/);
        push @{ $tracks->{lc($type)} }, {language => $language, name => $name};
    }

    return $tracks;
}

sub play {
    my ($self, $file) = @_;

    `echo '' > $self->{_control_file}`;

    open $self->{_fh}, '|-', "omxplayer -b -t 0 --align center --no-ghost-box -o hdmi '$file' >$self->{_out_file} 2>&1"
        or die "can't open";
    # binmode $self->{_fh};
    $self->{_fh}->autoflush();
    local $SIG{PIPE} = sub {die 'pipe broke'};
}

my %control_keys = (
    pause => ' ',
    stop  => chr(27),
);
sub control_play {
    my ($self, $command) = @_;

    my $control_key = $control_keys{$command};

    return unless defined $control_key;

    print { $self->{_fh} } $control_key;
    if ($command eq 'stop') {
        close $self->{_fh};
    }
}


1;
